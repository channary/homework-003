var startBtn = true;
var stopBtn = false;
var clearBtn = false;
var startTime = document.getElementById("start_time");
var stopTime = document.getElementById("stop_time");
var period = document.getElementById("period");
var payment = document.getElementById('payment');
var btnBox = document.getElementById('btn_box');
var btn = document.getElementById('btn');

function display_c(){
    var refresh=1000; // Refresh rate in milli seconds
    mytime=setTimeout('display_ct()',refresh)
    }

function display_ct() {
    var dateString = new Date().toDateString();
    var time = new Date().toLocaleTimeString();
    date = dateString + " " + time;
    document.getElementById('ct').innerHTML = date;
    display_c();
}

var start;
btn.addEventListener("click", function(){
    if(startBtn){
        btn.innerHTML = '<div class="inline-block bg-red-600 py-2 px-14 rounded-md text-white capitalize cursor-pointer" id="btnstop"><i class="fa fa-stop-circle pr-2"></i>stop</div>';
        start = new Date();
        var currentStartTime = start.toLocaleTimeString('en-US', {
            hour: '2-digit',            
            minute: '2-digit',            
            });
        startTime.innerHTML = currentStartTime;
        startBtn = false;
        stopBtn = true;
    }else if (stopBtn){
        btn.innerHTML = '<div class="inline-block bg-orange-600 py-2 px-14 rounded-md text-white capitalize cursor-pointer" id="btnclear"><i class="fa fa-trash pr-2" ></i>clear</div>';
        var stop = new Date();
        var currentStopTime = stop.toLocaleTimeString('en-US', {
            hour: '2-digit',            
            minute: '2-digit',            
            });
        stopTime.innerHTML = currentStopTime;
        total = Math.floor((stop - start)/60000);       
         
        // total = 250;
        period.innerHTML = total;
        var hours = Math.floor(total/60);
        var minutes = total % 60;
        // console.log(hours +" "+ minutes);        

        
        var pay;
        if( hours > 0 && minutes == 0){
            pay = hours*1500;
            payment.innerHTML = pay;
        }else if(minutes <= 15){
            pay = hours*1500 + 500;
            payment.innerHTML = pay;
        }else if(minutes <= 30){
            pay = hours*1500 + 1000;
            payment.innerHTML = pay;
        }
        else{
            pay = hours*1500 + 1500;
            payment.innerHTML = pay;
        }
        stopBtn = false;
        clearBtn = true;
    }else{
        btn.innerHTML = '<div class="inline-block bg-green py-2 px-14 rounded-md text-white capitalize cursor-pointer" id="btn"><i class="fa fa-play pr-2"></i>start</div></div>';
        startTime.innerHTML = "0:00";
        stopTime.innerHTML = "0:00";
        period.innerHTML = "0";
        payment.innerHTML = "0";
        clearBtn = false;
        startBtn = true;
    }
})





